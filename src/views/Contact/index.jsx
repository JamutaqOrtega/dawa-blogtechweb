import { useState } from "react";
import { TextField, Button, Container } from "@mui/material";

const Contact = () => {
  //se definen los valores iniciales de los inputs (1 solo state para todos los inputs)
  const [inputs, setInputs] = useState({
    name: "",
    email: "",
  });

  /**
   * Actualiza los valores de los inputs, recibe el evento tambien
   * @param {Event} e - evento que entrega el DOM
   */
  const handleInputChange = (e) => {
    //cuando es un input, este evento puede obtener el name y el value
    const { name, value } = e.target;

    setInputs({
      ...inputs,
      [name]: value,
    });
  };

  /**
   * Funcion para definir que hacer cuando se hace click en el boton submit
   * @param {Event} e
   */
  const handleSubmit = (e) => {
    //cuando es un submit, se puede usar el preventDefault (evitar que se recargue)
    e.preventDefault();
    console.log("name", inputs.name);
    console.log("email", inputs.email);
  };

  return (
    <>
        <Container maxWidth="xl">
          <div>
            <h1>Hola</h1>
          </div>
        </Container>
      {/* <h1>Contact</h1>
      <form action="">
        <p>
          <TextField
            type="text"
            name="name"
            label="Nombre"
            value={inputs.name}
            onChange={handleInputChange}
          />
        </p>
        <p>
          <TextField
            type="text"
            label="Email"
            name="email"
            value={inputs.email}
            onChange={handleInputChange}
          />
        </p>
        <p>
          <Button onClick={handleSubmit} variant="contained" type="submit">
            Enviar
          </Button>
        </p>
      </form> */}
    </>
  );
};

export default Contact;
