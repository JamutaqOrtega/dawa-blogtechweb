const NavBar = () => {
  return (
    <nav>
      <div className="nav-wrapper">
        <ul>
          <li>A</li>
          <li>B</li>
        </ul>
      </div>
    </nav>
  );
};

export default NavBar;
